package config;

import models.LinkedAccount;
import models.SecurityRole;
import models.User;

import java.util.List;

public class DataForStartup {

    public static void createStartupData() {

        List<User> usersInDb = User.find.all();
        if (0 != usersInDb.size()) {
            return;
        }

        System.out.println("Writing base data to database");

        SecurityRole curator = new SecurityRole(SecurityRole.CURATOR_ROLE);
        curator.save();

        User scottbeslow = new User("Scott Beslow", "bezzy21@yahoo.com");
        scottbeslow.save();
        LinkedAccount linkedAccount = new LinkedAccount();
        linkedAccount.user = scottbeslow;
        linkedAccount.providerKey = "password";
        linkedAccount.providerUserId = "$2a$10$n1ShqFrd2UMHn5Lzf8KV4OeJcq6hOkvvktXHzVbgkyF9kNm9HMX.a";
        linkedAccount.save();

    }
}
