package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public final static String FLASH_MESSAGE_KEY = "error";

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

}
