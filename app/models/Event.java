package models;

import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Event extends Model {

    @Id
    public long id;

    public String name;

    public DateTime startTime;

    public DateTime endTime;

    @ManyToMany (mappedBy = "events")
    List<Person> people;

    @ManyToMany (mappedBy = "events")
    List<Place> places;

}
