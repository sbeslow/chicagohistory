package models;

import org.joda.time.LocalDate;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Person extends Model {

    @Id
    public long id;

    public String firstName;

    public String lastName;

    public LocalDate birthDate;

    public LocalDate deathDate;

    @ManyToMany
    public List<Event> events;

}
