package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Place extends Model {

    @Id
    public long id;

    public String name;

    public String address;

    public Long lat;

    public Long lon;

    @ManyToMany
    public List<Event> events;

}
