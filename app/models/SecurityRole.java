package models;

import be.objectify.deadbolt.core.models.Role;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class SecurityRole extends Model implements Role {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    //public static final String RESIDENT_ROLE = "resident";
    public static final String CURATOR_ROLE = "curator";

    @Id
    public String roleName;

    @ManyToOne
    public User user;

    public SecurityRole(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String getName() {
        return roleName;
    }

    public static SecurityRole findByRoleName(String roleName) {
        return new SecurityRole(roleName);
    }
}