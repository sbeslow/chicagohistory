# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table event (
  id                        bigint not null,
  name                      varchar(255),
  start_time                timestamp,
  end_time                  timestamp,
  constraint pk_event primary key (id))
;

create table linked_account (
  id                        bigint not null,
  user_id                   bigint,
  provider_user_id          varchar(255),
  provider_key              varchar(255),
  constraint pk_linked_account primary key (id))
;

create table person (
  id                        bigint not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  birth_date                date,
  death_date                date,
  constraint pk_person primary key (id))
;

create table place (
  id                        bigint not null,
  name                      varchar(255),
  address                   varchar(255),
  lat                       bigint,
  lon                       bigint,
  constraint pk_place primary key (id))
;

create table security_role (
  role_name                 varchar(255) not null,
  user_id                   bigint,
  constraint pk_security_role primary key (role_name))
;

create table token_action (
  id                        bigint not null,
  token                     varchar(255),
  target_user_id            bigint,
  type                      varchar(2),
  created                   timestamp,
  expires                   timestamp,
  constraint ck_token_action_type check (type in ('EV','PR')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id))
;

create table users (
  id                        bigint not null,
  email                     varchar(255),
  name                      varchar(255),
  last_login                timestamp,
  active                    boolean,
  email_validated           boolean,
  accepted_by_admin         boolean,
  constraint pk_users primary key (id))
;

create table user_permission (
  id                        bigint not null,
  value                     varchar(255),
  constraint pk_user_permission primary key (id))
;


create table person_event (
  person_id                      bigint not null,
  event_id                       bigint not null,
  constraint pk_person_event primary key (person_id, event_id))
;

create table place_event (
  place_id                       bigint not null,
  event_id                       bigint not null,
  constraint pk_place_event primary key (place_id, event_id))
;

create table users_user_permission (
  users_id                       bigint not null,
  user_permission_id             bigint not null,
  constraint pk_users_user_permission primary key (users_id, user_permission_id))
;
create sequence event_seq;

create sequence linked_account_seq;

create sequence person_seq;

create sequence place_seq;

create sequence security_role_seq;

create sequence token_action_seq;

create sequence users_seq;

create sequence user_permission_seq;

alter table linked_account add constraint fk_linked_account_user_1 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_1 on linked_account (user_id);
alter table security_role add constraint fk_security_role_user_2 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_security_role_user_2 on security_role (user_id);
alter table token_action add constraint fk_token_action_targetUser_3 foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_targetUser_3 on token_action (target_user_id);



alter table person_event add constraint fk_person_event_person_01 foreign key (person_id) references person (id) on delete restrict on update restrict;

alter table person_event add constraint fk_person_event_event_02 foreign key (event_id) references event (id) on delete restrict on update restrict;

alter table place_event add constraint fk_place_event_place_01 foreign key (place_id) references place (id) on delete restrict on update restrict;

alter table place_event add constraint fk_place_event_event_02 foreign key (event_id) references event (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_02 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists event;

drop table if exists person_event;

drop table if exists place_event;

drop table if exists linked_account;

drop table if exists person;

drop table if exists place;

drop table if exists security_role;

drop table if exists token_action;

drop table if exists users;

drop table if exists users_user_permission;

drop table if exists user_permission;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists event_seq;

drop sequence if exists linked_account_seq;

drop sequence if exists person_seq;

drop sequence if exists place_seq;

drop sequence if exists security_role_seq;

drop sequence if exists token_action_seq;

drop sequence if exists users_seq;

drop sequence if exists user_permission_seq;

